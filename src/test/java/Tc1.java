
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Tc1 {
	private Logger log;
	private WebDriver driver;
	private Rozetka_index rozetka_index;
	private Rozetka_Smartfon_page rozetka_Smartfon_page;

	@Before
	public void setUp() {
		PropertyConfigurator.configure("log4j.properties");
		System.setProperty("webdriver.chrome.driver", "chromedriver_win32/chromedriver1");
		this.driver = new ChromeDriver();
//	  	System.setProperty("webdriver.ie.driver", "IEDriverServer_x64_3.3.0/IEDriverServer.exe");
//	  	this.driver = new InternetExplorerDriver();
		rozetka_index = new Rozetka_index(driver);
		rozetka_Smartfon_page = new Rozetka_Smartfon_page(driver);
	}
	@Test
	public void test1() {
		rozetka_index.openRozetka();
		Assert.assertTrue("The step not passed", rozetka_index.moveToElementLinkTelefony_tv_i_ehlektronika());
		Assert.assertTrue("The step not passed", rozetka_index.clickOnLinkSmartfon());
		Assert.assertTrue("The step not passed", rozetka_Smartfon_page.clicOnDiagonal());
		Assert.assertTrue("The step not passed", rozetka_Smartfon_page.typeTextToInput_price_max("6000"));
		Assert.assertTrue("The step not passed", rozetka_Smartfon_page.clickOnElement(rozetka_Smartfon_page.getButton_ok()));
		Assert.assertTrue("The step not passed", rozetka_Smartfon_page.clicOnProducer());
		Assert.assertTrue("The step not passed", rozetka_Smartfon_page.findElementCount(rozetka_Smartfon_page.getGoods_item_with_promotion()));
		Assert.assertTrue("The step not passed", rozetka_Smartfon_page.findElementCount(rozetka_Smartfon_page.getXpathFromValue(rozetka_Smartfon_page.getValue("src", rozetka_Smartfon_page.getFirst_element()))));
		Assert.assertTrue("The step not passed", rozetka_Smartfon_page.clickOnElement(rozetka_Smartfon_page.getDD()));
		Assert.assertTrue("The step not passed", rozetka_Smartfon_page.clickOnElement(rozetka_Smartfon_page.getDDpopularity()));
		Assert.assertTrue("The step not passed", rozetka_Smartfon_page.findElementAndClickOn(rozetka_Smartfon_page.getRemembering_first_element()));
		Assert.assertTrue("The step not passed", rozetka_Smartfon_page.showAssessment(rozetka_Smartfon_page.getValue("style", rozetka_Smartfon_page.getAssessment())));
	}
	@After
	public void testDown() {
		rozetka_Smartfon_page.closePage();	
	}
}

