
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Rozetka_index {

	private Logger log;
	private WebDriver driver;

	public Rozetka_index(WebDriver driver) {
		this.driver = driver;
		log = Logger.getLogger(getClass());
		PageFactory.initElements(driver, this);
	}

	@FindBy (xpath  = ".//*[@id='3361']/a")
	WebElement linkTelefony_tv_i_ehlektronika;

	@FindBy (xpath  = ".//*[@href='https://rozetka.com.ua/mobile-phones/c80003/filter/preset=smartfon/']")
	WebElement linkSmartfon;

//	String goods_item_with_promotion = ".//*[@name='goods_item_with_promotion']";
//	
//	public String getGoods_item_with_promotion() {
//		return goods_item_with_promotion;
//	}

	public void openRozetka() {
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		try {
			driver.get("https://rozetka.com.ua/");
			log.info("Browser and url was opened");
		} catch (Exception e) {
			Assert.fail("Can not work with browser");
		}
	}

	public void closePage() {
		driver.quit();
	}

	public boolean moveToElementLinkTelefony_tv_i_ehlektronika() {
		try {
			Actions actions = new Actions(driver);
			actions.moveToElement(linkTelefony_tv_i_ehlektronika).build().perform();
			try {
				actions.moveToElement(linkSmartfon).build().perform();
			} catch (Exception e) {
				linkTelefony_tv_i_ehlektronika.click();
			}
			log.info("Element find and move");
			return true;
		} catch (Exception e) {
			log.error("Can not move to element " + e);
			return false;
		}
	}

	public boolean clickOnLinkSmartfon() {
		try {
			linkSmartfon.click();
			log.info("Element was clicked");
			return true;
		} catch (Exception e) {
			log.error("Element was not click");
			return false;
		}
	}
}
