
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Rozetka_Smartfon_page {
	
	private Logger log;
	private WebDriver driver;
	private WebDriverWait wait;

	public Rozetka_Smartfon_page(WebDriver driver) {
		this.driver = driver;
		log = Logger.getLogger(Rozetka_Smartfon_page.class);
		PageFactory.initElements(driver, this);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 30);
	}
	@FindBy (xpath  = ".//*[@id='price[max]']")
	private WebElement input_price_max;
	
	@FindBy (xpath  = ".//*[@id='submitprice']")
	private WebElement button_ok;
	
	@FindBy (xpath  = ".//*[@id='filter_23777_50069']//a")
	private WebElement input_diagonal4_1;
	
	@FindBy (xpath  = ".//*[@id='filter_23777_50074']//a")
	private WebElement input_diagonal_4_5;
	
	@FindBy (xpath  = ".//*[@id='filter_23777_93089']//a")
	private WebElement input_diagonal_5;
	
	@FindBy (xpath  = ".//*[@id='filter_23777_114812']//a")
	private WebElement input_diagonal_5_5;
	
	@FindBy (xpath  = ".//*[@id='filter_23777_25316']//a")
	private WebElement input_diagonal_6;
	
	@FindBy (xpath  = ".//*[@id='filter_producer_66']//a")
	private WebElement htc;
	
	@FindBy (xpath  = ".//*[@id='filter_producer_4']//a")
	private WebElement asus;
	
	@FindBy (xpath  = ".//*[@id='filter_producer_511']//a")
	private WebElement huawei;
	
	@FindBy (xpath  = ".//*[@id='filter_producer_120']//a")
	private WebElement lenovo;
	
	@FindBy (xpath  = ".//*[@id='filter_producer_12']//a")
	private WebElement samsung;
	
	@FindBy (xpath  = ".//*[@id='filter_producer_57']//a")
	private WebElement prestigio;

	@FindBy (xpath  = ".//*[@id='sort_producer']/../a[1]")
	private WebElement sortProducer;
	
	@FindBy (xpath  = ".//*[@id='filter_producer_21']//a")
	private WebElement fly;
	
	@FindBy (xpath  = ".//*[@id='filter_producer_118']//a")
	private WebElement assistant;
	
	@FindBy (xpath  = ".//*[@id='catalog_goods_block']/div/div[1]/div[1]/div/div[1]/div[2]/div[1]/div[1]/a/img")
	private WebElement first_element;
	
	@FindBy (xpath  = ".//*[@id='sort_view']/a")
	private WebElement DD;
	
	@FindBy (xpath  = ".//*[@id='filter_sortpopularity']/a")
	private WebElement DDpopularity;
	
	@FindBy (xpath  = ".//*[@class='g-rating-stars-i-medium sprite']")
	private WebElement assessment;
	
	private String goods_item_with_promotion = ".//*[@class='sprite g-rating-stars-i']";
	
	private static String remembering_first_element;
	
	public String getRemembering_first_element() {
		return remembering_first_element;
	}
	public String 	getGoods_item_with_promotion() {
		return goods_item_with_promotion;
	}
	public WebElement getDDpopularity() {
		return DDpopularity;
	}
	public WebElement getButton_ok() {
		return button_ok;
	}
	public WebElement getDD() {
		return DD;
	}
	public WebElement getFirst_element() {
		return first_element;
	}
	public WebElement getAssessment() {
		return assessment;
	}
	public boolean clicOnDiagonal() {
		try {
			Thread.sleep(3000);
			input_diagonal4_1.click();
			Thread.sleep(3000);
			input_diagonal_4_5.click();
			Thread.sleep(3000);
			input_diagonal_5.click();
			Thread.sleep(3000);
			input_diagonal_5_5.click();
			Thread.sleep(3000);
			input_diagonal_6.click();
			log.info("diagona 4.1-4.5, 4.5-5.0, 5.1-5.5, 5.55-6.0, 6.0+ was selected");
			return true;
		} catch (Exception e) {
			log.error("Can not work with checkbox " + e);
			return false;
		}
	}
	public By getByOfElement(WebElement webElement) {
		return By.xpath(getXpath(webElement));
	}

	public String getXpath(WebElement webElement) {
		String line = webElement.toString();
		String substring = line.substring(line.lastIndexOf("xpath:") + 6, line.length() - 1);
		return substring.trim();
	}

	public boolean clicOnProducer() {
		try {
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(getByOfElement(sortProducer)))).click();
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(getByOfElement(htc)))).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(getByOfElement(sortProducer)))).click();
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(getByOfElement(asus)))).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(getByOfElement(sortProducer)))).click();
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(getByOfElement(huawei)))).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(getByOfElement(sortProducer)))).click();
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(getByOfElement(lenovo)))).click();
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(getByOfElement(sortProducer)))).click();
			wait.until(ExpectedConditions.elementToBeClickable(
					driver.findElement(getByOfElement(samsung)))).click();
			log.info("htc,asus,huawei,lenovo,samsung was selected");
			return true;
			} catch (Exception e) {
				log.error("Can not work with checkbox " + e);
				return false;
		}
	}
	public String getValue(String atribut, WebElement element) {
		try {
			Thread.sleep(2000);
			wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(getByOfElement(element)));
			String tempValue = element.getAttribute(atribut);
			log.info("Get new value " + tempValue);
			return tempValue;
		} catch (Exception e) {
			log.error("Temp value it is not possible to get");
			return "";
		}
	}
	public boolean showAssessment(String value) {
		try {
			if (value.equals("width: 100%;")) {
				log.info("Assessment = 5.0");
				return true;
			}
			else if (value.equals("width: 90%;")) {
				log.info("Assessment = 4.5");
				return true;
			}
			else if (value.equals("width: 80%;")) {
				log.info("Assessment = 4.0");
				return true;
			}
			else if (value.equals("width: 70%;")) {
				log.info("Assessment = 3.5");
				return true;
			}
			else if (value.equals("width: 60%;")) {
				log.info("Assessment = 3.0");
				return true;
			}
			else if (value.equals("width: 50%;")) {
				log.info("Assessment = 2.5");
				return true;
			}
			else if (value.equals("width: 40%;")) {
				log.info("Assessment = 2.0");
				return true;
			}
			else if (value.equals("width: 30%;")) {
				log.info("Assessment = 1.5");
				return true;
			}
			else if (value.equals("width: 20%;")) {
				log.info("Assessment = 1.0");
				return true;
			}
			else if (value.equals("width: 10%;")) {
				log.info("Assessment = 0.5");
				return true;
			}
			else if(value.equals("width: 0%;")){
				log.info("Assessment = 0.0");
				return true;
			}
			else {
				log.info("Assessment was not find");
				return false;
			}
		} catch (Exception e) {
			log.error("Assessment was not find " + e);
			return false;
		}
	}
	public String getXpathFromValue(String value) {
		return remembering_first_element = ".//*[@src='" + value + "']";
	}
	public boolean findElementCount(String xpath) {
		int count = 0 ;		
		try {
			Thread.sleep(2000);
			List<WebElement> element = driver.findElements(By.xpath(xpath));
			count = element.size();
			if(count > 1) {
				log.info( count + " elements was found");
				return true;
			}
			else if (count > 0 ) {
				log.info( count + " element was found");
				return true;
			}
			else return false;
		} catch (Exception e) {
			log.error("elements was find " + e);
			return false;
		}
	}
	public boolean findElementAndClickOn(String xpath) {
		try {
			Thread.sleep(3000);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,3500)", "");
			List<WebElement> element = driver.findElements(By.xpath(xpath));
			Thread.sleep(3000);
			WebElement d = element.get(0);
			Thread.sleep(3000);
			d.click();
			log.info("Element found and was clicked");
			return true;
		} catch (Exception e) {
			log.error("Element was not find or was not click");
			return false;
		}
	}
	public boolean typeTextToInput_price_max(String text) {
		try {
			Thread.sleep(2000);
			input_price_max.clear();
			input_price_max.sendKeys(text);
			log.info(text + " was sent");
			return true;
		} catch (Exception e) {
			log.error(text + " was not sent");
			return false;
		}	
	}
	public void closePage() {
		driver.quit();
	}
	public boolean clickOnElement(WebElement element) {
		try {
			element.click();
			log.info("Element was clicked");
			return true;
		} catch (Exception e) {
			log.error("Element was not click");
			return false;
		}
	}
}
